package com.softserveinc.ita.manygame.engine.mill;


import com.softserveinc.ita.manygame.engine.GameState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class MillTurnLogicValidationTest {
    private Mill mill;
    private String firstPlayer;
    private String secondPlayer;

    @Before
    public void setUp() {
        mill = new Mill();
        secondPlayer = "second";
        firstPlayer = "first";
        mill.setFirstPlayer(firstPlayer);
        mill.setSecondPlayer(secondPlayer);
    }

    @Test
    public void WhenFirstPlayerPutAndHeHasStonesInHand() {
        String turn = Commands.PUT_NEW_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertTrue(response);
    }

    @Test
    public void WhenFirstPlayerPutAndHeDoesNotHaveStonesInHand() {
        Player p = mill.firstPlayer;
        for (int i = 0; i < 9; i++) {
            p.getNewStone();
        }
        String turn = Commands.PUT_NEW_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenFirstPlayerPutsStoneToNotFreePosition() {
        String turn = Commands.PUT_NEW_STONE + 5;
        mill.changeGameState(secondPlayer, turn);

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenTurnIsDropAddFirstPlayerPutsStone() {
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_DROP);
        String turn = Commands.PUT_NEW_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenFirstPlayerHasStonesInHandAndReplaceHisStone() {
        mill.changeGameState(firstPlayer, Commands.PUT_NEW_STONE + 5);
        String turn = Commands.REPLACE_STONE + 5 + " " + 6;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }


    @Test
    public void WhenFirstPlayerReplaceHisStoneToFreeNeighborPosition() {
        Player p = mill.firstPlayer;
        for (int i = 0; i < 8; i++) {
            p.getNewStone();
        }
        mill.changeGameState(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        String turn = Commands.REPLACE_STONE + 5 + " " + 6;

        boolean response = mill.validateTurnLogic(turn);

        assertTrue(response);
    }

    @Test
    public void WhenTurnIsDropAndFirstPlayerReplaceHisStone() {
        Player p = mill.firstPlayer;
        for (int i = 0; i < 8; i++) {
            p.getNewStone();
        }
        mill.changeGameState(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_DROP);

        String turn = Commands.REPLACE_STONE + 5 + " " + 6;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenFirstPlayerReplaceHisStoneToNotFreeNeighborPosition() {
        Player p = mill.firstPlayer;
        for (int i = 0; i < 8; i++) {
            p.getNewStone();
        }
        mill.changeGameState(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + 6);
        String turn = Commands.REPLACE_STONE + 5 + " " + 6;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenFirstPlayerReplaceHisStoneNotToNeighborPosition() {
        Player p = mill.firstPlayer;
        for (int i = 0; i < 8; i++) {
            p.getNewStone();
        }
        mill.changeGameState(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        String turn = Commands.REPLACE_STONE + 5 + " " + 14;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenFirstPlayerHasOnlyThreeStoneAndReplaceHisStoneNotToNeighborPosition() {
        Player p = mill.firstPlayer;
        for (int i = 0; i < 6; i++) {
            p.dropStone(p.getNewStone());
        }
        for (int i = 0; i < 2; i++) {
            p.getNewStone();
        }
        mill.changeGameState(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_TURN);
        String turn = Commands.REPLACE_STONE + 5 + " " + 14;

        boolean response = mill.validateTurnLogic(turn);

        assertTrue(response);
    }

    @Test
    public void WhenTurnIsNotDropAndFirstPlayerDropOpponentsStone() {
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 5);
        String turn = Commands.DROP_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenTurnIsDropAndFirstPlayerDropHisOwnStone() {
        mill.changeGameState(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_DROP);
        String turn = Commands.DROP_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenTurnIsDropAndFirstPlayerDropFreePosition() {
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_DROP);
        String turn = Commands.DROP_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);

    }

    @Test
    public void WhenTurnIsDropAndFirstPlayerDropOpponentsStone() {
        String previousTurn = Commands.PUT_NEW_STONE + " " + 5;
        mill.changeGameState(secondPlayer, previousTurn);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_DROP);
        String turn = Commands.DROP_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertTrue(response);
    }

    @Test
    public void WhenTurnIsDropAndFirstPlayerDropOpponentsStoneInMill() {
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 4);
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 5);
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 6);
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 7);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_DROP);
        String turn = Commands.DROP_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertFalse(response);
    }

    @Test
    public void WhenTurnIsDropAndFirstPlayerDropOpponentsStoneInMillBecauseThereIsNotAnotherStones() {
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 4);
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 5);
        mill.changeGameState(secondPlayer, Commands.PUT_NEW_STONE + " " + 6);
        mill.setGameState(GameState.WAIT_FOR_FIRST_PLAYER_DROP);
        String turn = Commands.DROP_STONE + 5;

        boolean response = mill.validateTurnLogic(turn);

        assertTrue(response);
    }

}
