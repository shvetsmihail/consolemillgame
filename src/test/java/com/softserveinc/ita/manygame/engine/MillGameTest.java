package com.softserveinc.ita.manygame.engine;

import com.softserveinc.ita.manygame.engine.mill.Commands;
import com.softserveinc.ita.manygame.engine.mill.Mill;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MillGameTest {
    private Mill mill;
    private String firstPlayer;
    private String secondPlayer;

    @Before
    public void setUp() {
        mill = new Mill();
        firstPlayer = "first";
        secondPlayer = "second";
        mill.setFirstPlayer(firstPlayer);
        mill.setSecondPlayer(secondPlayer);
    }

    @Test
    public void WhenFirstPlayerMakeValidPut(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);

        int expected = GameResultCode.OK;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakeValidPut(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);

        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 6);

        int expected = GameResultCode.OK;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenSecondPlayerPutHisStoneToNotFreePosition(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);

        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenIsNotDropTurnAndFirstPlayerDropStone(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 6);

        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 6);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakePutOutOfTurn(){
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);

        int expected = GameResultCode.BAD_TURN_ORDER;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakeReplaceOutOfTurn(){
        mill.makeTurn(secondPlayer, Commands.REPLACE_STONE + 5 + " " + 6);

        int expected = GameResultCode.BAD_TURN_ORDER;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakeDropOutOfTurn(){
        mill.makeTurn(secondPlayer, Commands.DROP_STONE + 5);

        int expected = GameResultCode.BAD_TURN_ORDER;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerReplaceStoneWhenHeHasStonesInHand(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 6);

        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 5 + " " + 7);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMill(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);

        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)

        int expected = GameState.WAIT_FOR_FIRST_PLAYER_DROP;
        int actual = mill.gameState;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndThenHePutStone(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)

        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 8);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndThenHeReplaceStone(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)

        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 4 + " " + 8);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndDropHisOwnStone(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)

        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 4);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndDropEmptyPosition(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)

        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 12);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndDropOpponentStone(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)

        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 0);

        int expected = GameResultCode.OK;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndSecondPlayerTryToMakeTurn(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)

        mill.makeTurn(secondPlayer, Commands.DROP_STONE + 4);

        int expected = GameResultCode.BAD_TURN_ORDER;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenPlayerDropOpponentStoneInMillWhenThereAreOpponentStoneOutOfMill(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 14);    //out of mill (14)
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 2);    //secondPlayer create mill (0 1 2)
        mill.makeTurn(secondPlayer, Commands.DROP_STONE + 1);

        mill.makeTurn(secondPlayer, Commands.DROP_STONE + 4);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerDropOpponentStoneInMillWhenThereAreNotOpponentStoneOutOfMill(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);     //firstPlayer create mill (4 5 6)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 1);
        mill.gameState = GameState.WAIT_FOR_SECOND_PLAYER_TURN;
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 2);    //secondPlayer create mill (0 1 2)
        mill.makeTurn(secondPlayer, Commands.DROP_STONE + 1);

        mill.makeTurn(secondPlayer, Commands.DROP_STONE + 4);

        int expected = GameResultCode.OK;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerPutStoneWhenHisHandEmpty(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 2);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 3);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 6);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 7);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 8);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 9);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 10);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 11);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 12);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 13);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 14);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 15);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 16);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 18);    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 19);   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 20);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeValidReplace(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 2);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 3);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 6);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 7);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 8);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 9);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 10);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 11);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 12);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 13);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 14);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 15);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 16);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 18);    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 19);   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 0);

        int expected = GameResultCode.OK;
        int actual = mill.resultCode;
        assertEquals(expected, actual);

    }

    @Test
    public void WhenFirstPlayerReplaceStoneToNotFreePosition(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 2);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 3);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 6);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 7);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 8);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 9);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 10);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 11);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 12);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 13);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 14);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 15);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 16);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 18);    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 19);   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 2);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerReplaceStoneToNotNeighborPositionWhenHeHasMoreThen3StoneInGame(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 2);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 3);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 6);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 7);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 8);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 9);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 10);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 11);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 12);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 13);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 14);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 15);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 16);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 18);    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 19);   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 22);

        int expected = GameResultCode.BAD_TURN_LOGIC;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenSecondPlayerReplaceStoneToNotNeighborPositionWhenHeHasOly3StoneInGame(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 2);             //firstPlayer create mill (0 1 2)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 8 stones;
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 7);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 3);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);             //firstPlayer create mill (0 7 6)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 7 stones;
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 15);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 11);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 23);            //firstPlayer create mill (7 15 23)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 6 stones;
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 8);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 13);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 10);            // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 20);           // Doesn't have stone in hand
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 9);   //firstPlayer create mill (8 9 10)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 5 stones;
        mill.makeTurn(secondPlayer, Commands.REPLACE_STONE + 13 + " " + 5);
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 9 + " " + 1);   //firstPlayer create mill (0 1 2)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 4 stones;
        mill.makeTurn(secondPlayer, Commands.REPLACE_STONE + 4 + " " + 5);
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 9);   //firstPlayer create mill (8 9 10)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 5);                 //secondPlayer has 3 stones;

        mill.makeTurn(secondPlayer, Commands.REPLACE_STONE + 11 + " " + 5);

        int expected = GameResultCode.OK;
        int actual = mill.resultCode;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerReplaceHisStoneAndDestroyMillButCreateNewMill(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 2);             //firstPlayer create mill (0 1 2)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 7);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 3);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 15);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 11);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 23);
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 8);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 13);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 10);            // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 20);           // Doesn't have stone in hand

        //firstPlayer destroy mill (0 1 2) and create mill (8 9 10)
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 9);

        int expected = GameState.WAIT_FOR_FIRST_PLAYER_DROP;
        int actual = mill.gameState;
        assertEquals(expected, actual);
    }

    @Test
    public void WhenFirstPlayerDropOpponentStoneAndOpponentHas2Stones(){
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 0);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 4);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 1);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 2);             //firstPlayer create mill (0 1 2)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 8 stones;
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 7);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 3);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 6);             //firstPlayer create mill (0 7 6)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 7 stones;
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 15);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 11);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 23);            //firstPlayer create mill (7 15 23)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 6 stones;
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 5);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 8);
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 13);
        mill.makeTurn(firstPlayer, Commands.PUT_NEW_STONE + 10);            // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, Commands.PUT_NEW_STONE + 20);           // Doesn't have stone in hand
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 9);   //firstPlayer create mill (8 9 10)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 5 stones;
        mill.makeTurn(secondPlayer, Commands.REPLACE_STONE + 13 + " " + 5);
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 9 + " " + 1);   //firstPlayer create mill (0 1 2)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 4 stones;
        mill.makeTurn(secondPlayer, Commands.REPLACE_STONE + 4 + " " + 5);
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 1 + " " + 9);   //firstPlayer create mill (8 9 10)
        mill.makeTurn(firstPlayer, Commands.DROP_STONE + 5);                 //secondPlayer has 3 stones;
        mill.makeTurn(secondPlayer, Commands.REPLACE_STONE + 11 + " " + 5);
        mill.makeTurn(firstPlayer, Commands.REPLACE_STONE + 9 + " " + 1);   //firstPlayer create mill (0 1 2)

        mill.makeTurn(firstPlayer, Commands.DROP_STONE +5);                 //secondPlayer has 2 stones;

        int expected = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        int actual = mill.gameState;
        assertEquals(expected, actual);
    }
}
