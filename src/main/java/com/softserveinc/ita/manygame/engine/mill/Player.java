package com.softserveinc.ita.manygame.engine.mill;


import java.util.ArrayList;
import java.util.List;

class Player {
    private List<Stone> stonesInHand = new ArrayList<>();
    private List<Stone> stonesInGame = new ArrayList<>();
    private String color;

    String getColor() {
        return color;
    }

    Player(String color) {
        this.color = color;
        initStones();
    }

    Stone getNewStone(){
        Stone stone = stonesInHand.remove(0);
        stonesInGame.add(stone);
        return stone;
    }

    void dropStone(Stone stone){
        stonesInGame.remove(stone);
    }

    int getCountOfStones() {
        return stonesInGame.size() + stonesInHand.size();
    }
    int getCountOfStonesInHand() {
        return stonesInHand.size();
    }
    int getCountOfStonesInGame () {
        return stonesInGame.size();
    }

    private void initStones(){
        for (int i = 0; i < 9; i++) {
            stonesInHand.add(new Stone(color));
        }
    }

}
