package com.softserveinc.ita.manygame.engine.mill;

import com.softserveinc.ita.manygame.engine.GameState;
import com.softserveinc.ita.manygame.engine.GenericGameEngine;

public class Mill extends GenericGameEngine {
    private Field field = new Field();
    Player firstPlayer = new Player("white");
    Player secondPlayer = new Player("black");

    public Mill() {
        super(1L);
    }

    @Override
    public String getBoard() {
        return field.drawField();
    }

    @Override
    protected boolean validateTurnOrder(String playerName) {
        boolean firstPlayerCorrectOrder = firstPlayerName.equals(playerName)
                && (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_FIRST_PLAYER_DROP);

        boolean secondPlayerCorrectOrder = secondPlayerName.equals(playerName)
                && (gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_DROP);
        return firstPlayerCorrectOrder || secondPlayerCorrectOrder;
    }

    @Override
    public boolean isStarted() {
        return gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_FIRST_PLAYER_DROP
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_DROP;
    }

    protected boolean validateTurnSyntax(String turn) {
        String command;

        if (turn.startsWith(Commands.PUT_NEW_STONE)) {
            command = Commands.PUT_NEW_STONE;
        } else if (turn.startsWith(Commands.DROP_STONE)) {
            command = Commands.DROP_STONE;
        } else if (turn.startsWith(Commands.REPLACE_STONE)) {
            command = Commands.REPLACE_STONE;
        } else {
            return false;
        }

        turn = turn.substring(command.length()).trim();

        String[] split = turn.split("[ ]+");

        if (command.equals(Commands.REPLACE_STONE)) {
            if (split.length != 2) {
                return false;
            }
        } else {
            if (split.length != 1) {
                return false;
            }
        }

        boolean isDigit = true;
        for (String s : split) {
            isDigit = isDigit && isDigitWithin1To23(s);
        }
        return isDigit;
    }

    protected boolean validateTurnLogic(String turn) {
        Player player = null;
        if (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_FIRST_PLAYER_DROP){
            player = firstPlayer;
        } else {
            player = secondPlayer;
        }

        if (turn.startsWith(Commands.PUT_NEW_STONE)) {
            return putLogicValidation(player, turn);
        }
        if (turn.startsWith(Commands.REPLACE_STONE)) {
            return replaceLogicValidation(player, turn);
        }
        if (turn.startsWith(Commands.DROP_STONE)) {
            return dropLogicValidation(player, turn);
        }

        return false;
    }

    protected boolean validatePlayerName(String playerName) {
        return true;
    }

    protected int changeGameState(String playerName, String turn) {
        doTurn(playerName, turn);

        if (field.isNewMillCreate()) {
            System.out.println("create mill. Count: " + field.getCountOfMill());
            if (playerName.equals(getFirstPlayer())) {
                return GameState.WAIT_FOR_FIRST_PLAYER_DROP;
            } else {
                return GameState.WAIT_FOR_SECOND_PLAYER_DROP;
            }
        }

        if (firstPlayer.getCountOfStones() < 3) {
            return GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        }
        if (secondPlayer.getCountOfStones() < 3) {
            return GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        }

        if (playerName.equals(getFirstPlayer())) {
            return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
        } else {
            return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
        }
    }

    private void doTurn(String playerName, String turn) {
        Player player;
        Player opponent;

        if (playerName.equals(getFirstPlayer())) {
            player = firstPlayer;
            opponent = secondPlayer;
        } else {
            player = secondPlayer;
            opponent = firstPlayer;
        }

        int[] positions = turnParser(turn);

        if (turn.startsWith(Commands.PUT_NEW_STONE)) {
            putStone(player.getNewStone(), positions[0]);
            System.out.print("put: ");
        }
        if (turn.startsWith(Commands.REPLACE_STONE)) {
            replaceStone(positions[0], positions[1]);
            System.out.print("replace: ");
        }
        if (turn.startsWith(Commands.DROP_STONE)) {
            opponent.dropStone(dropStone(positions[0]));
            System.out.print("drop: ");
        }
        System.out.println(player.getColor() + ": hand:" + player.getCountOfStonesInHand() + " game:"
                             + player.getCountOfStonesInGame() + " total:" + player.getCountOfStones());
    }

    //only for tests
    void setGameState(int state){
        gameState = state;
    }

    private void putStone(Stone stone, int position) {
        Position p = field.getPosition(position);
        p.setStone(stone);
    }

    private void replaceStone(int oldPosition, int newPosition) {
        Position oldP = field.getPosition(oldPosition);
        Stone stone = oldP.getStone();
        oldP.setStone(null);
        if (field.isMillDestroy()) {
            System.out.println("destroy mill. Count: " + field.getCountOfMill());
        }
        Position newP = field.getPosition(newPosition);
        newP.setStone(stone);
    }

    private Stone dropStone(int position) {
        Position p = field.getPosition(position);
        Stone stone = p.getStone();
        p.setStone(null);
        if (field.isMillDestroy()) {
            System.out.println("destroy mill. Count: " + field.getCountOfMill());
        }
        return stone;
    }

    private int[] turnParser(String turn) {

        String command = null;

        if (turn.startsWith(Commands.PUT_NEW_STONE)) {
            command = Commands.PUT_NEW_STONE;
        }
        if (turn.startsWith(Commands.DROP_STONE)) {
            command = Commands.DROP_STONE;
        }
        if (turn.startsWith(Commands.REPLACE_STONE)) {
            command = Commands.REPLACE_STONE;
        }

        turn = turn.substring(command.length()).trim();

        String[] split = turn.split("[ ]+");
        int[] position = new int[split.length];

        for (int i = 0; i < split.length; i++) {
            position[i] = Integer.parseInt(split[i]);
        }

        return position;
    }

    private boolean isDigitWithin1To23(String string) {
        try {
            int i = Integer.parseInt(string);
            return (i >= 0) && (i <= 23);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean putLogicValidation(Player player, String turn) {
        int[] positions = turnParser(turn);
        Position p = field.getPosition(positions[0]);
        return  player.getCountOfStonesInHand() > 0
                && p.getStone() == null
                && (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN);
    }

    private boolean replaceLogicValidation(Player player, String turn) {
        int[] positions = turnParser(turn);
        Position oldP = field.getPosition(positions[0]);
        Position newP = field.getPosition(positions[1]);
        return  player.getCountOfStonesInHand() == 0
                && oldP.getStone() != null
                && oldP.getStone().getColor().equals(player.getColor())
                && newP.getStone() == null
                && (oldP.getNeighbors().contains(newP) || player.getCountOfStones() == 3)
                && (gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN);
    }

    private boolean dropLogicValidation(Player player, String turn) {
        int[] positions = turnParser(turn);
        Position p = field.getPosition(positions[0]);

        boolean isAllPositionInMill = true;
        for (Position position : field.getPositions()){
            if (position.getStone() != null && !position.getStone().getColor().equals(player.getColor())){
                isAllPositionInMill = isAllPositionInMill && field.isPositionInMill(position);
            }
        }

        return p.getStone() != null && !p.getStone().getColor().equals(player.getColor())
                && (!field.isPositionInMill(p) || isAllPositionInMill)
                && (gameState == GameState.WAIT_FOR_FIRST_PLAYER_DROP
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_DROP);
    }
}
