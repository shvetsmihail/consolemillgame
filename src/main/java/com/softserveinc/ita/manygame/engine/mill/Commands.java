package com.softserveinc.ita.manygame.engine.mill;


public class Commands {
    public static final String PUT_NEW_STONE = "p";
    public static final String REPLACE_STONE = "r";
    public static final String DROP_STONE = "d";
}
