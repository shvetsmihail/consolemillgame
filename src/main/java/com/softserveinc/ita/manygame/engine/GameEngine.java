package com.softserveinc.ita.manygame.engine;

import java.util.Map;

public interface GameEngine {
    Long getId();
    boolean setFirstPlayer(String playerName);
    String getFirstPlayer();
    boolean setSecondPlayer(String playerName);
    String getSecondPlayer();
    boolean makeTurn(String playerName, String turn);
    int getResultCode();
    boolean isFinished();
    boolean isStarted();  //After the Player names are set correctly
    String getBoard();
    String getTheWinner();
    boolean init(String playerName, Map<String, Object> initData);
    Object getInfo();
}
