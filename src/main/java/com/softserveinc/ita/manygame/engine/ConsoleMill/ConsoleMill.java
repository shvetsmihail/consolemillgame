package com.softserveinc.ita.manygame.engine.ConsoleMill;


import com.softserveinc.ita.manygame.engine.mill.Mill;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleMill {
    private static String firstPlayer;
    private static String secondPlayer;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Mill mill = new Mill();

        System.out.println("> enter first player name");
        firstPlayer = reader.readLine();
        mill.setFirstPlayer(firstPlayer);

        System.out.println("> enter second player name");
        secondPlayer = reader.readLine();
        mill.setSecondPlayer(secondPlayer);

        while (!mill.isFinished()) {
            System.out.println("> enter: name cmd{p,v,d} p1{0-23} p2{0-23}(only if you choose cdm 'v')");
            String[] command = consoleParser(reader.readLine());
            if (command == null){
                System.out.println("bad command");
                continue;
            }
            mill.makeTurn(command[0], command[1]);
            System.out.println(mill.getBoard());
        }

        System.out.println("> winner is " + mill.getTheWinner());

    }

    private static String[] consoleParser(String line) {
        String playerName;
        if (line.startsWith(firstPlayer)) {
            playerName = firstPlayer;
        } else if (line.startsWith(secondPlayer)) {
            playerName = secondPlayer;
        } else {
            return null;
        }

        String[] command = new String[2];
        line = line.substring(playerName.length()).trim();

        command[0] = playerName;
        command[1] = line;

        return command;
    }
}
